package com.atlassian.connect.spring.it.conditional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyIterable;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
public class ConditionalOnResourceIT {

    @Autowired
    private WebApplicationContext wac;

    @Test
    public void shouldNotRegisterAnyBeansWithoutAddonDescriptorPresent() {
        assertThat(getStarterBeanClasses(), is(emptyIterable()));
    }

    private List<? extends Class<?>> getStarterBeanClasses() {
        return Arrays.stream(wac.getBeanDefinitionNames())
                .map(wac::getType)
                .filter(this::isStarterClass)
                .collect(Collectors.toList());
    }

    private boolean isStarterClass(Class c) {
        Package classPackage = c.getPackage();
        return classPackage != null
                && classPackage.getName().startsWith("com.atlassian.connect.spring")
                && !DescriptorLessApplication.class.isAssignableFrom(c);
    }
}
