package com.atlassian.connect.spring.it.data;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.it.util.AtlassianHostBuilder;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AtlassianHostRepositoryAuditingIT extends BaseApplicationIT {

    private static final String USER_KEY = "charlie";

    @Before
    public void setUp() {
        setJwtAuthenticatedPrincipal(new AtlassianHostBuilder().clientKey(randomAlphanumeric(32)).build(), USER_KEY);
    }

    @Test
    public void shouldStoreAuditingFields() {
        AtlassianHost host = new AtlassianHostBuilder()
                .clientKey(randomAlphanumeric(32))
                .build();
        hostRepository.save(host);

        AtlassianHost readHost = hostRepository.findOne(host.getClientKey());
        assertThat(readHost.getCreatedDate(), is(notNullValue()));
        assertThat(readHost.getCreatedBy(), equalTo(USER_KEY));
        assertThat(readHost.getLastModifiedDate(), is(notNullValue()));
        assertThat(readHost.getLastModifiedBy(), equalTo(USER_KEY));
    }
}
