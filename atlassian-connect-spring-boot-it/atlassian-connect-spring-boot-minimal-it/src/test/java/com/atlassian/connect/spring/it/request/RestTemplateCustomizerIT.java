package com.atlassian.connect.spring.it.request;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.AtlassianHostUser.AtlassianHostUserBuilder;
import com.atlassian.connect.spring.internal.request.oauth2.OAuth2RestTemplateFactory;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Optional;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestTemplateCustomizerIT extends BaseApplicationIT {

    private static DummyClientHttpRequestInterceptor dummyRequestInterceptor = new DummyClientHttpRequestInterceptor();

    @Autowired
    private AtlassianHostRestClients atlassianHostRestClients;

    @Autowired
    private OAuth2RestTemplateFactory oAuth2RestTemplateFactory;

    private AtlassianHost host;

    @Before
    public void setUp() {
        host = createAndSaveHost(hostRepository);
    }

    @Test
    public void shouldCustomizeJwtRestTemplate() {
        assertRestTemplateHasBadRequestInterceptor(atlassianHostRestClients.authenticatedAsAddon());
    }

    @Test
    public void shouldCustomizeOAuth2RestTemplate() {
        AtlassianHostUser hostUser = AtlassianHostUser.builder(host)
                .withUserAccountId("cab9a26e-56ec-49e9-a08f-d7e4a19bde55").build();
        assertRestTemplateHasBadRequestInterceptor(atlassianHostRestClients.authenticatedAs(hostUser));
    }

    @Test
    public void shouldCustomizeAccessTokenProviderRestTemplate() {
        assertRestTemplateHasBadRequestInterceptor(oAuth2RestTemplateFactory.getAccessTokenProviderRestTemplate());
    }

    private void assertRestTemplateHasBadRequestInterceptor(RestTemplate restTemplate) {
        assertThat(restTemplate.getInterceptors(), hasItem(dummyRequestInterceptor));
    }

    @TestConfiguration
    public static class RestTemplateCustomizerConfiguration {

        @Bean
        public RestTemplateCustomizer badInterceptorCustomizer() {
            return restTemplate -> restTemplate.getInterceptors().add(dummyRequestInterceptor);
        }
    }

    public static class DummyClientHttpRequestInterceptor implements ClientHttpRequestInterceptor {

        @Override
        public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
            throw new UnsupportedOperationException("Not implemented");
        }
    }
}
